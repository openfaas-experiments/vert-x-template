plugins {
	id("java-library")
	kotlin("jvm") version "1.3.0"
}

dependencies {
	val VERTX_VER = "3.5.4"
  val KOTLIN_VER = "1.3.0"
	implementation(kotlin(module = "stdlib-jdk8", version = KOTLIN_VER))

  compile("io.vertx:vertx-web-client:$VERTX_VER")
  //compile("io.kotlintest:kotlintest:$KOTLIN_TEST_VER")
  compile("io.vertx:vertx-core:$VERTX_VER")
  compile("io.vertx:vertx-lang-kotlin:$VERTX_VER")
  compile("io.vertx:vertx-web:$VERTX_VER")
}

repositories {
  jcenter()
}
