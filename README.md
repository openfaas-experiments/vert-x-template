# OpenFaaS Vert.x template

This template provides additional context and control over the HTTP response from your function.

## Status of the template

This template is pre-release and is likely to change

## Supported platforms

* x86_64 - `java8-vert-x`
* x86_64 - `kotlin-vert-x`

## Trying the template

> Java + Vert.x

```
$ faas template pull https://gitlab.com/openfaas-experiments/vert-x-template
$ faas new hello-vert-x --lang java8-vert-x
```

> Kotlin + Vert.x

```
$ faas template pull https://gitlab.com/openfaas-experiments/vert-x-template
$ faas new hello-vert-x --lang kotlin-vert-x
```

> **Remark**: where `hello-vert-x` is the name of your function

## Example usage

> WIP: 🚧

### Java sample

```java
package com.openfaas.function;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.core.json.JsonObject;

public class Handler implements BodyHandler {

  @Override
  public void handle(RoutingContext routingContext) {
    routingContext.response()
      .putHeader("content-type", "application/json;charset=UTF-8")
      .end(
        new JsonObject()
          .put("message", "👋 Hello 🌍 World")
          .put("remark", "Java version")
          .encodePrettily()
      );
  }

  @Override
  public BodyHandler setBodyLimit(long bodyLimit) {
    return null;
  }

  @Override
  public BodyHandler setUploadsDirectory(String uploadsDirectory) {
    return null;
  }

  @Override
  public BodyHandler setMergeFormAttributes(boolean mergeFormAttributes) {
    return null;
  }

  @Override
  public BodyHandler setDeleteUploadedFilesOnEnd(boolean deleteUploadedFilesOnEnd) {
    return null;
  }

}

```

### Kotlin sample

```kotlin
package com.openfaas.function

import io.vertx.core.http.HttpServerResponse
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.core.json.JsonObject

class Handler:BodyHandler {
  override fun handle(routingContext:RoutingContext) {
    routingContext.response()
      .putHeader("content-type", "application/json;charset=UTF-8")
      .end(
        JsonObject()
          .put("message", "👋 Hello 🌍 World")
          .put("remark", "Kotlin version")
          .encodePrettily()
      )
  }
  override fun setBodyLimit(bodyLimit:Long):BodyHandler? {
    return null
  }
  override fun setUploadsDirectory(uploadsDirectory:String):BodyHandler? {
    return null
  }
  override fun setMergeFormAttributes(mergeFormAttributes:Boolean):BodyHandler? {
    return null
  }
  override fun setDeleteUploadedFilesOnEnd(deleteUploadedFilesOnEnd:Boolean):BodyHandler? {
    return null
  }
}
```

## Developing the template

### Contribution rules

> WIP :construction:

### Testing

There are 2 demo functions:

- `test-j` (Java version)
- `test-k` (Kotlin version)

For testing on your own platform, you need to export 2 environment variables:

```shell
export OPENFAAS_URL=http://openfaas.test:8080
export REGISTRY_DOMAIN=registry.test:5000
```

> `REGISTRY_DOMAIN` could be your own docker registry or your DockerHub handle

And generate the appropriate yaml deployment files for each function:

```shell
envsubst < test-j.yml > try-j.yml
envsubst < test-k.yml > try-k.yml
```

> Remark, to install `envsubst` on OSX, use these commands:
> - `brew install gettext`
> - `brew link --force gettext`

#### Build, push, deploy

```shell
# Kotlin
faas-cli build -f try-k.yml
faas-cli push -f try-k.yml
faas-cli deploy -f try-k.yml
#Java
faas-cli build -f try-j.yml
faas-cli push -f try-j.yml
faas-cli deploy -f try-j.yml
```


