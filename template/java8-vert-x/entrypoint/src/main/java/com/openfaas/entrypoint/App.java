// Copyright (c) OpenFaaS Author(s) 2018. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
package com.openfaas.entrypoint;

import io.vertx.core.http.HttpServer;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.BodyHandler;
import java.util.Optional;

public class App {
  public static void main(String[] args) throws Exception {
    Vertx vertx = Vertx.vertx();
    Integer httpPort = Integer.parseInt(Optional.ofNullable(System.getenv("PORT")).orElse("8081"));
    HttpServer server = vertx.createHttpServer();
    Router router = Router.router(vertx);

    /**
     * to serve only html assets, change your deploy yaml file:
     * add a variable `FRONTAPP` to the environment key:
     * 
     *  environment:
     *    FRONTAPP: false
     * 
     * if FRONTAPP is true the function will serve the assets from
     * the `/src/main/resources/webroot` directory
     * 
     * if FRONTAPP is false the function will serve data from `com.openfaas.function.Handler`
     */
    if (Boolean.parseBoolean(Optional.ofNullable(System.getenv("FRONTAPP")).orElse("false"))) {
      // serve static assets, see /resources/webroot directory
      router.route("/*").handler(StaticHandler.create());
    } else {
      BodyHandler handler = new com.openfaas.function.Handler();
      router.route().handler(handler);
    }

    server.requestHandler(router::accept).listen(httpPort, result -> {
      if(result.succeeded()) {
        System.out.println("🌍 Listening on " + httpPort);
      } else {
        System.out.println("😡 Houston, we have a problem: " + result.cause().getMessage());
      }
    });

  }

}
