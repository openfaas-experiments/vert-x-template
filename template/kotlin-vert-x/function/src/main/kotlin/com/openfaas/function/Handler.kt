package com.openfaas.function

import io.vertx.core.http.HttpServerResponse
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.core.json.JsonObject

class Handler:BodyHandler {
  override fun handle(routingContext:RoutingContext) {
    routingContext.response()
      .putHeader("content-type", "application/json;charset=UTF-8")
      .end(
        JsonObject()
          .put("message", "👋 Hello 🌍 World")
          .put("remark", "Kotlin version")
          .encodePrettily()
      )
  }
  override fun setBodyLimit(bodyLimit:Long):BodyHandler? {
    return null
  }
  override fun setUploadsDirectory(uploadsDirectory:String):BodyHandler? {
    return null
  }
  override fun setMergeFormAttributes(mergeFormAttributes:Boolean):BodyHandler? {
    return null
  }
  override fun setDeleteUploadedFilesOnEnd(deleteUploadedFilesOnEnd:Boolean):BodyHandler? {
    return null
  }
}